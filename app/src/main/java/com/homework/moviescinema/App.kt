package com.homework.moviescinema

import android.app.Application
import com.homework.moviescinema.base_screen.di.baseScreenModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            modules(appModule, baseScreenModule)
        }
        Timber.plant(Timber.DebugTree())
    }
}