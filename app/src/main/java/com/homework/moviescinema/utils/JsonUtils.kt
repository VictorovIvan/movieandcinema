package com.homework.moviescinema.utils

import com.homework.moviescinema.detail_screen.data.model.Movie
import com.homework.moviescinema.detail_screen.data.model.Review
import com.homework.moviescinema.detail_screen.data.model.Trailer
import org.json.JSONException
import org.json.JSONObject

class JsonUtils {
    val KEY_RESULTS = "results"

    //Для отзывов
    val KEY_AUTHOR = "author"
    val KEY_CONTENT = "content"

    //Для видео
    val KEY_KEY_OF_VIDEO = "key"
    val KEY_NAME = "name"
    val BASE_YOUTUBE_URL = "https://www.youtube.com/watch?v="

    //Вся информация о фильме
    val KEY_VOTE_COUNT = "vote_count"
    val KEY_ID = "id"
    val KEY_TITLE = "title"
    val KEY_ORIGINAL_TITLE = "original_title"
    val KEY_OVERVIEW = "overview"
    val KEY_POSTER_PATH = "poster_path"
    val KEY_BACKDROP_PATH = "backdrop_path"
    val KEY_VOTE_AVERAGE = "vote_average"
    val KEY_RELEASE_DATE = "release_date"
    val BASE_POSTER_URL = "https://image.tmdb.org/t/p/"
    val SMALL_POSTER_SIZE = "w342"
    val BIG_POSTER_SIZE = "w780"
    public fun getReviewsFromJSON(jsonObject: JSONObject?): ArrayList<Review> {
        val result: ArrayList<Review> = ArrayList()
        if (jsonObject == null) {
            return result
        }
        try {
            val jsonArray = jsonObject.getJSONArray(KEY_RESULTS)
            for (i in 0 until jsonArray.length()) {
                val jsonObjectReview = jsonArray.getJSONObject(i)
                val author = jsonObjectReview.getString(KEY_AUTHOR)
                val content = jsonObjectReview.getString(KEY_CONTENT)
                val review = Review(author, content)
                result.add(review)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return result
    }

    public fun getTrailersFromJSON(jsonObject: JSONObject?): ArrayList<Trailer> {
        val result: ArrayList<Trailer> = ArrayList()
        if (jsonObject == null) {
            return result
        }
        try {
            val jsonArray = jsonObject.getJSONArray(KEY_RESULTS)
            for (i in 0 until jsonArray.length()) {
                val jsonObjectTrailers = jsonArray.getJSONObject(i)
                val key = BASE_YOUTUBE_URL + jsonObjectTrailers.getString(KEY_KEY_OF_VIDEO)
                val name = jsonObjectTrailers.getString(KEY_NAME)
                val trailer = Trailer(key, name)
                result.add(trailer)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return result
    }

    public fun getMoviesFromJSON(jsonObject: JSONObject?): ArrayList<Movie> {
        val result: ArrayList<Movie> = ArrayList()
        if (jsonObject == null) {
            return result
        }
        try {
            val jsonArray = jsonObject.getJSONArray(KEY_RESULTS)
            for (i in 0 until jsonArray.length()) {
                val objectMovie = jsonArray.getJSONObject(i)

                val id = objectMovie.getInt(KEY_ID)
                val voteCount = objectMovie.getInt(KEY_VOTE_COUNT)
                val title = objectMovie.getString(KEY_TITLE)
                val originalTitle = objectMovie.getString(KEY_ORIGINAL_TITLE)
                val overview = objectMovie.getString(KEY_OVERVIEW)
                val posterPath = BASE_POSTER_URL + SMALL_POSTER_SIZE + objectMovie.getString(
                    KEY_POSTER_PATH
                )
                val bigPosterPath = BASE_POSTER_URL + BIG_POSTER_SIZE + objectMovie.getString(
                    KEY_POSTER_PATH
                )
                val backdropPath = objectMovie.getString(KEY_BACKDROP_PATH)
                val voteAverage = objectMovie.getDouble(KEY_VOTE_AVERAGE)
                val releaseDate = objectMovie.getString(KEY_RELEASE_DATE)
                val movie = Movie(
                    0,
                    id,
                    voteCount,
                    title,
                    originalTitle,
                    overview,
                    posterPath,
                    bigPosterPath,
                    backdropPath,
                    voteAverage,
                    releaseDate
                )
                result.add(movie)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return result
    }
}
