package com.homework.moviescinema.utils

import android.content.Context
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import androidx.loader.content.AsyncTaskLoader
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.util.concurrent.ExecutionException


object NetworkUtils {
    private const val BASE_URL = "https://api.themoviedb.org/3/discover/movie"
    private const val BASE_URL_VIDEOS = "https://api.themoviedb.org/3/movie/%s/videos"
    private const val BASE_URL_REVIEWS = "https://api.themoviedb.org/3/movie/%s/reviews"
    private const val PARAMS_API_KEY = "api_key"
    private const val PARAMS_LANGUAGE = "language"
    private const val PARAMS_SORT_BY = "sort_by"
    private const val PARAMS_PAGE = "page"
    private const val PARAMS_MIN_VOTE_COUNT = "vote_count.gte"
    private const val API_KEY = "3811dffbd8bc87f702dad54554f272a9"
    private const val LANGUAGE_VALUE = "ru-RU"
    private const val SORT_BY_POPULARITY = "popularity.desc"
    private const val SORT_BY_TOP_RATED = "vote_average.desc"
    private const val MIN_VOTE_COUNT_VALUE = "1000"
    const val POPULARITY = 0
    const val TOP_RATED = 1
    fun buildURLToVideos(id: Int): URL? {
        val uri = Uri.parse(String.format(BASE_URL_VIDEOS, id)).buildUpon()
            .appendQueryParameter(PARAMS_API_KEY, API_KEY)
            .appendQueryParameter(PARAMS_LANGUAGE, LANGUAGE_VALUE).build()
        try {
            return URL(uri.toString())
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return null
    }

    fun buildURLToReviews(id: Int): URL? {
        val uri = Uri.parse(String.format(BASE_URL_REVIEWS, id)).buildUpon()
            .appendQueryParameter(PARAMS_API_KEY, API_KEY).build()
        try {
            return URL(uri.toString())
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return null
    }

    fun buildURL(sortBy: Int, page: Int): URL? {
        var result: URL? = null
        val methodOfSort: String
        methodOfSort = if (sortBy == POPULARITY) {
            SORT_BY_POPULARITY
        } else {
            SORT_BY_TOP_RATED
        }
        val uri = Uri.parse(BASE_URL).buildUpon()
            .appendQueryParameter(PARAMS_API_KEY, API_KEY)
            .appendQueryParameter(PARAMS_LANGUAGE, LANGUAGE_VALUE)
            .appendQueryParameter(PARAMS_SORT_BY, methodOfSort)
            .appendQueryParameter(PARAMS_MIN_VOTE_COUNT, MIN_VOTE_COUNT_VALUE)
            .appendQueryParameter(PARAMS_PAGE, Integer.toString(page))
            .build()
        try {
            result = URL(uri.toString())
        } catch (e: MalformedURLException) {
            e.printStackTrace()
        }
        return result
    }

    fun getJSONForVideos(id: Int): JSONObject? {
        var result: JSONObject? = null
        val url = buildURLToVideos(id)
        try {
            result = JSONLoadTask().execute(url).get()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return result
    }

    fun getJSONForReviews(id: Int): JSONObject? {
        var result: JSONObject? = null
        val url = buildURLToReviews(id)
        try {
            result = JSONLoadTask().execute(url).get()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return result
    }

    fun getJSONFromNetwork(sortBy: Int, page: Int): JSONObject? {
        var result: JSONObject? = null
        val url = buildURL(sortBy, page)
        try {
            result = JSONLoadTask().execute(url).get()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return result
    }

    class JSONLoader(context: Context, private val bundle: Bundle?) :
        AsyncTaskLoader<JSONObject?>(context) {
        private var onStartLoadingListener: OnStartLoadingListener? = null

        interface OnStartLoadingListener {
            fun onStartLoading()
        }

        fun setOnStartLoadingListener(onStartLoadingListener: OnStartLoadingListener?) {
            this.onStartLoadingListener = onStartLoadingListener
        }

        override fun onStartLoading() {
            super.onStartLoading()
            if (onStartLoadingListener != null) {
                onStartLoadingListener!!.onStartLoading()
            }
            forceLoad()
        }

        override fun loadInBackground(): JSONObject? {
            if (bundle == null) {
                return null
            }
            val urlAsString = bundle.getString("url")
            var url: URL? = null
            try {
                url = URL(urlAsString)
            } catch (e: MalformedURLException) {
                e.printStackTrace()
            }
            var result: JSONObject? = null
            if (url == null) {
                return null
            }
            var connection: HttpURLConnection? = null
            try {
                connection = url.openConnection() as HttpURLConnection
                val inputStream = connection.inputStream
                val inputStreamReader = InputStreamReader(inputStream)
                val reader = BufferedReader(inputStreamReader)
                val builder = StringBuilder()
                var line = reader.readLine()
                while (line != null) {
                    builder.append(line)
                    line = reader.readLine()
                }
                result = JSONObject(builder.toString())
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: JSONException) {
                e.printStackTrace()
            } finally {
                connection?.disconnect()
            }
            return result
        }
    }

    private class JSONLoadTask :
        AsyncTask<URL?, Void?, JSONObject?>() {
        override fun doInBackground(vararg urls: URL?): JSONObject? {
            var result: JSONObject? = null
            if (urls == null || urls.size == 0) {
                return null
            }
            var connection: HttpURLConnection? = null
            try {
                connection = urls[0]?.openConnection() as HttpURLConnection
                val inputStream = connection.inputStream
                val inputStreamReader = InputStreamReader(inputStream)
                val reader = BufferedReader(inputStreamReader)
                val builder = StringBuilder()
                var line = reader.readLine()
                while (line != null) {
                    builder.append(line)
                    line = reader.readLine()
                }
                result = JSONObject(builder.toString())
            } catch (e: IOException) {
                e.printStackTrace()
            } catch (e: JSONException) {
                e.printStackTrace()
            } finally {
                connection?.disconnect()
            }
            return result
        }
    }
}
