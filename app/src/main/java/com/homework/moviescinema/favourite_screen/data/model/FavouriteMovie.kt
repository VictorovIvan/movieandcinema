package com.homework.moviescinema.favourite_screen.data.model

import androidx.room.Entity
import androidx.room.Ignore
import com.homework.moviescinema.detail_screen.data.model.Movie

@Entity(tableName = "favourite_movies")
class FavouriteMovie : Movie {
    constructor(
        uniqueId: Int,
        id: Int,
        voteCount: Int,
        title: String?,
        originalTitle: String?,
        overview: String?,
        posterPath: String?,
        bigPosterPath: String?,
        backdropPath: String?,
        voteAverage: Double,
        releaseDate: String?
    ) : super(
        uniqueId, id, voteCount,
        title!!,
        originalTitle!!,
        overview!!,
        posterPath!!,
        bigPosterPath!!,
        backdropPath!!,
        voteAverage,
        releaseDate!!
    )

    @Ignore
    constructor(movie: Movie) : super(
        movie.uniqueId,
        movie.id,
        movie.voteCount,
        movie.title,
        movie.originalTitle,
        movie.overview,
        movie.posterPath,
        movie.bigPosterPath,
        movie.backdropPath,
        movie.voteAverage,
        movie.releaseDate
    ) {
    }
}