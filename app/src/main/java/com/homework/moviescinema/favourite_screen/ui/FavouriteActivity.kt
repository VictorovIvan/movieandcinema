package com.homework.moviescinema.favourite_screen.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import com.homework.moviescinema.detail_screen.adapter.MovieAdapter
import com.homework.moviescinema.detail_screen.adapter.MovieAdapter.OnPosterClickListener
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import com.homework.moviescinema.favourite_screen.data.model.FavouriteMovie
import com.homework.moviescinema.base_screen.ui.BaseViewModel
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.homework.moviescinema.detail_screen.ui.DetailActivity
import com.homework.moviescinema.base_screen.ui.BaseActivity
import com.homework.moviescinema.R
import com.homework.moviescinema.detail_screen.data.model.Movie


class FavouriteActivity : AppCompatActivity() {

    lateinit var recyclerViewFavouriteMovies: RecyclerView
    lateinit var adapter: MovieAdapter
    lateinit var viewModel: BaseViewModel

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.getItemId()
        when (id) {
            R.id.itemMain -> {
                val intent = Intent(this, BaseActivity::class.java)
                startActivity(intent)
            }
            R.id.itemFavourite -> {
                val intentToFavourite = Intent(this, FavouriteActivity::class.java)
                startActivity(intentToFavourite)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favourite)
        recyclerViewFavouriteMovies = findViewById(R.id.recyclerViewFavouriteMovies)
        recyclerViewFavouriteMovies.layoutManager = GridLayoutManager(this, 1)
        adapter = MovieAdapter()
        recyclerViewFavouriteMovies.adapter = adapter
        viewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)
        var favouriteMovies = viewModel!!.favouriteMovies
        favouriteMovies!!.observe(this, object : Observer<List<FavouriteMovie?>?> {
            override fun onChanged(t: List<FavouriteMovie?>?) {
                var movies: MutableList<Movie> = ArrayList()
                if (favouriteMovies != null) {
                    movies = addAll(favouriteMovies) as MutableList<Movie>
                    adapter!!.setMovies(movies)
                }
            }
        })
        adapter!!.setOnPosterClickListener(object : OnPosterClickListener {
            override fun onPosterClick(position: Int) {
                val movie: Movie = adapter!!.getMovies()[position]
                val intent = Intent(this@FavouriteActivity, DetailActivity::class.java)
                intent.putExtra("id", movie.id)
                startActivity(intent)
            }
        })
    }

}

private fun addAll(favouriteMovies: LiveData<List<FavouriteMovie?>?>): List<Movie?>? {
    return favouriteMovies.value
}


