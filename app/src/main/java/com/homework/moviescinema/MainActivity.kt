package com.homework.moviescinema

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.homework.moviescinema.base_screen.ui.BaseActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)
        Intent(this, BaseActivity::class.java).also {
            startActivity(it)
        }
    }
}