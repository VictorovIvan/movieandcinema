package com.homework.moviescinema.base_screen.data.api

import com.homework.moviescinema.detail_screen.data.model.Movie
import com.homework.moviescinema.detail_screen.domain.model.MoviesDomainModel

interface MoviesRepo {
    suspend fun getMovies(methodOfSort: Int, page: Int): ArrayList<MoviesDomainModel>
}