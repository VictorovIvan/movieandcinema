package com.homework.moviescinema.base_screen.data.api

import com.homework.moviescinema.detail_screen.data.model.Movie

class MoviesRemoteSource (
    private val api: MoviesApi
) {
    suspend fun getMovies(methodOfSort: Int, page: Int): ArrayList<Movie> {

        return api.getMovies(
            methodOfSort = methodOfSort,
            page = page
        )
    }
}