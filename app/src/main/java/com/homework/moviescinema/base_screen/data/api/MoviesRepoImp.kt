package com.homework.moviescinema.base_screen.data.api

import com.homework.moviescinema.base_screen.data.toDomain
import com.homework.moviescinema.detail_screen.domain.model.MoviesDomainModel

class MoviesRepoImp (
    private val moviesDomainMode: MoviesRemoteSource
    ) : MoviesRepo {
    override suspend fun getMovies(methodOfSort: Int, page: Int): ArrayList<MoviesDomainModel> {
        return moviesDomainMode.getMovies(methodOfSort, page).toDomain()
    }
}


