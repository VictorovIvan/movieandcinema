package com.homework.moviescinema.base_screen.ui

import android.app.Application
import android.os.AsyncTask
import androidx.annotation.NonNull

import androidx.lifecycle.LiveData

import androidx.lifecycle.AndroidViewModel
import com.homework.moviescinema.detail_screen.data.model.Movie
import com.homework.moviescinema.detail_screen.data.model.MovieDatabase
import com.homework.moviescinema.favourite_screen.data.model.FavouriteMovie
import java.util.concurrent.ExecutionException


class BaseViewModel(@NonNull application: Application?) :
    AndroidViewModel(application!!) {
    val movies: LiveData<List<Movie?>?>?
    var favouriteMovies: LiveData<List<FavouriteMovie?>?>?

    fun getMovieById(id: Int): Movie? {
        try {
            return GetMovieTask().execute(id).get()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return null
    }

    fun getFavouriteMovieById(id: Int): FavouriteMovie? {
        try {
            return GetFavouriteMovieTask().execute(id).get()
        } catch (e: ExecutionException) {
            e.printStackTrace()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
        return null
    }

    fun deleteAllMovies() {
        DeleteMoviesTask().execute()
    }

    fun insertMovie(movie: Movie?) {
        InsertTask().execute(movie)
    }

    fun deleteMovie(movie: Movie?) {
        DeleteTask().execute(movie)
    }

    fun insertFavouriteMovie(movie: FavouriteMovie?) {
        InsertFavouriteTask().execute(movie)
    }

    fun deleteFavouriteMovie(movie: FavouriteMovie?) {
        DeleteFavouriteTask().execute(movie)
    }

    private class DeleteFavouriteTask :
        AsyncTask<FavouriteMovie?, Void?, Void?>() {
        override fun doInBackground(vararg movies: FavouriteMovie?): Void? {
            if (movies != null && movies.size > 0) {
                database!!.movieDao()!!.deleteFavouriteMovie(movies[0])
            }
            return null
        }
    }

    private class InsertFavouriteTask :
        AsyncTask<FavouriteMovie?, Void?, Void?>() {
        override fun doInBackground(vararg movies: FavouriteMovie?): Void? {
            if (movies != null && movies.size > 0) {
                database!!.movieDao()!!.insertFavouriteMovie(movies[0])
            }
            return null
        }
    }

    private class DeleteTask :
        AsyncTask<Movie?, Void?, Void?>() {
        override fun doInBackground(vararg movies: Movie?): Void? {
            if (movies != null && movies.size > 0) {
                database!!.movieDao()!!.deleteMovie(movies[0])
            }
            return null
        }
    }

    private class InsertTask :
        AsyncTask<Movie?, Void?, Void?>() {
        override fun doInBackground(vararg movies: Movie?): Void? {
            if (movies != null && movies.size > 0) {
                database!!.movieDao()!!.insertMovie(movies[0])
            }
            return null
        }
    }

    private class DeleteMoviesTask :
        AsyncTask<Void?, Void?, Void?>() {
        override fun doInBackground(vararg params: Void?): Void? {
            database!!.movieDao()!!.deleteAllMovies()
            return null
        }
    }

    private class GetMovieTask : AsyncTask<Int?, Void?, Movie?>() {
        override fun doInBackground(vararg integers: Int?): Movie? {
            return if (integers != null && integers.size > 0) {
                integers[0]?.let { database!!.movieDao()!!.getMovieById(it) }
            } else null
        }
    }

    private class GetFavouriteMovieTask :
        AsyncTask<Int?, Void?, FavouriteMovie?>() {
        override fun doInBackground(vararg integers: Int?): FavouriteMovie? {
            return if (integers != null && integers.size > 0) {
                integers[0]?.let { database!!.movieDao()!!.getFavouriteMovieById(it) }
            } else null
        }
    }

    companion object {
        private var database: MovieDatabase? = null
    }

    init {
        database = MovieDatabase.getInstance(getApplication())
        movies = database!!.movieDao()!!.allMovies
        favouriteMovies = database!!.movieDao()!!.allFavouriteMovies
    }
}