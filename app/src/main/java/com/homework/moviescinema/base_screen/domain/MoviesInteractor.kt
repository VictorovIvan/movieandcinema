package com.homework.moviescinema.base_screen.domain

import com.homework.moviescinema.base_screen.data.api.MoviesRepo
import com.homework.moviescinema.detail_screen.domain.model.MoviesDomainModel

class MoviesInteractor (private  val moviesRepo: MoviesRepo ) {
    suspend fun getMovies(methodOfSort: Int, page: Int): ArrayList<MoviesDomainModel> {
        return moviesRepo.getMovies(methodOfSort, page)
    }
}