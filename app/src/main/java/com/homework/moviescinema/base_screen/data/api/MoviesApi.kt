package com.homework.moviescinema.base_screen.data.api

import com.homework.moviescinema.detail_screen.data.model.Movie
import retrofit2.http.GET
import retrofit2.http.Query

interface MoviesApi {
    @GET("discover/movie")
    suspend fun getMovies(
        @Query("api_key") cityName: String = "3811dffbd8bc87f702dad54554f272a9",
        @Query("language") units: String = "ru-RU",
        @Query("sort_by") methodOfSort: Int,
        @Query("vote_count.gte") vote_count: Int = 1000,
        @Query("page") page: Int
    ): ArrayList<Movie>
}