package com.homework.moviescinema.base_screen.di

import com.homework.moviescinema.base_screen.data.api.MoviesApi
import com.homework.moviescinema.base_screen.data.api.MoviesRemoteSource
import com.homework.moviescinema.base_screen.domain.MoviesInteractor
import org.koin.dsl.module
import retrofit2.Retrofit

val baseScreenModule = module {

    single {
        get<Retrofit>().create(MoviesApi::class.java)
    }

    single {
        MoviesRemoteSource(api = get())
    }

    single {
        MoviesInteractor(moviesRepo = get())
    }
}