package com.homework.moviescinema.base_screen.const
object HttpRoutes {
    const val BASE_URL = "https://api.themoviedb.org/3/"
}