package com.homework.moviescinema.base_screen.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.Switch
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.loader.app.LoaderManager
import androidx.loader.content.Loader
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.homework.moviescinema.R
import com.homework.moviescinema.detail_screen.adapter.MovieAdapter
import com.homework.moviescinema.detail_screen.adapter.MovieAdapter.OnPosterClickListener
import com.homework.moviescinema.detail_screen.data.model.Movie
import com.homework.moviescinema.detail_screen.ui.DetailActivity
import com.homework.moviescinema.favourite_screen.ui.FavouriteActivity
import com.homework.moviescinema.utils.JsonUtils
import com.homework.moviescinema.utils.NetworkUtils
import com.homework.moviescinema.utils.NetworkUtils.JSONLoader
import org.json.JSONObject
import java.net.URL


class BaseActivity : AppCompatActivity(),
    LoaderManager.LoaderCallbacks<JSONObject> {
    @SuppressLint("UseSwitchCompatOrMaterialCode")
    lateinit var switchSort: Switch
    private lateinit var recyclerViewPosters: RecyclerView
    private lateinit var movieAdapter: MovieAdapter
    private lateinit var textViewTopRated: TextView
    private lateinit var textViewPopularity: TextView
    private lateinit var progressBarLoading: ProgressBar
    private lateinit var viewModel: BaseViewModel
    private lateinit var loaderManager: LoaderManager
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        when (id) {
            R.id.itemMain -> {
                val intent = Intent(this, BaseActivity::class.java)
                startActivity(intent)
            }
            R.id.itemFavourite -> {
                val intentToFavourite = Intent(this, FavouriteActivity::class.java)
                startActivity(intentToFavourite)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loaderManager = LoaderManager.getInstance(this)
        viewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)
        switchSort = findViewById(R.id.switchSort)
        textViewPopularity = findViewById(R.id.textViewPopularity)
        textViewTopRated = findViewById(R.id.textViewTopRated)
        progressBarLoading = findViewById(R.id.progressBarLoading)
        recyclerViewPosters = findViewById(R.id.recyclerViewPosters)
        recyclerViewPosters.layoutManager = GridLayoutManager(this, 1)
        movieAdapter = MovieAdapter()
        recyclerViewPosters.setAdapter(movieAdapter)
        switchSort.setChecked(true)
        switchSort.setOnCheckedChangeListener { _, isChecked ->
            page = 1
            setMethodOfSort(isChecked)
        }
        switchSort.setChecked(false)
        movieAdapter!!.setOnPosterClickListener(object : OnPosterClickListener {
            override fun onPosterClick(position: Int) {
                val movie = movieAdapter!!.getMovies()[position]
                val intent = Intent(this@BaseActivity, DetailActivity::class.java)
                intent.putExtra("id", movie.id)
                startActivity(intent)
            }
        })
        movieAdapter!!.setOnReachEndListener(object : MovieAdapter.OnReachEndListener {
            override fun onReachEnd() {
                if (!isLoading) {
                    downloadData(methodOfSort, page)
                }
            }
        })
        val moviesFromLiveData = viewModel!!.movies
        moviesFromLiveData!!.observe(this, object : Observer<List<Movie?>?> {
            override fun onChanged(@Nullable movies: List<Movie?>?) {
                if (page == 1) {
                    movieAdapter!!.setMovies(movies as MutableList<Movie>)
                }
            }
        })
    }

    fun onClickSetPopularity(view: View?) {
        setMethodOfSort(false)
        switchSort!!.isChecked = false
    }

    fun onClickSetTopRated(view: View?) {
        setMethodOfSort(true)
        switchSort!!.isChecked = true
    }

    private fun setMethodOfSort(isTopRated: Boolean) {
        val networkUtils = NetworkUtils
        if (isTopRated) {
            textViewTopRated!!.setTextColor(resources.getColor(R.color.teal_200))
            textViewPopularity!!.setTextColor(resources.getColor(R.color.white_color))
            methodOfSort = networkUtils.TOP_RATED
        } else {
            methodOfSort = networkUtils.POPULARITY
            textViewPopularity!!.setTextColor(resources.getColor(R.color.teal_200))
            textViewTopRated!!.setTextColor(resources.getColor(R.color.white_color))
        }
        downloadData(methodOfSort, page)
    }

    private fun downloadData(methodOfSort: Int, page: Int) {
        val networkUtils = NetworkUtils
        val url: URL? = networkUtils.buildURL(methodOfSort, page)
        val bundle = Bundle()
        bundle.putString("url", url.toString())
        loaderManager!!.restartLoader(LOADER_ID, bundle, this)
    }

    override fun onCreateLoader(i: Int, @Nullable bundle: Bundle?): JSONLoader {
        val jsonLoader = JSONLoader(this, bundle)
        jsonLoader.setOnStartLoadingListener(object : JSONLoader.OnStartLoadingListener {
            override fun onStartLoading() {
                progressBarLoading!!.visibility = View.VISIBLE
                isLoading = true
            }
        })
        return jsonLoader
    }

    override fun onLoadFinished(loader: Loader<JSONObject>, jsonObject: JSONObject) {
        val jsonUtils = JsonUtils()
        val movies: ArrayList<Movie> = jsonUtils.getMoviesFromJSON(jsonObject)
        if (movies != null && !movies.isEmpty()) {
            if (page == 1) {
                viewModel!!.deleteAllMovies()
                movieAdapter.clear()
            }
            for (movie in movies) {
                viewModel!!.insertMovie(movie)
            }
            movieAdapter!!.addMovies(movies)
            page++
        }
        isLoading = false
        progressBarLoading!!.visibility = View.INVISIBLE
        loaderManager!!.destroyLoader(LOADER_ID)
    }

    override fun onLoaderReset(loader: Loader<JSONObject>) {}

    companion object {
        private const val LOADER_ID = 133
        private var page = 1
        private var methodOfSort = 0
        private var isLoading = false
    }
}