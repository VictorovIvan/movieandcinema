package com.homework.moviescinema.base_screen.ui

import androidx.lifecycle.ViewModel
import com.homework.moviescinema.base_screen.domain.MoviesInteractor

class BaseScreenViewModel(
    private val moviesInteractor: MoviesInteractor
) : ViewModel() {
}