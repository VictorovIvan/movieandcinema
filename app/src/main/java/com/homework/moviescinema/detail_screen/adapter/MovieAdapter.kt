package com.homework.moviescinema.detail_screen.adapter
import com.homework.moviescinema.detail_screen.adapter.MovieAdapter.MovieViewHolder


import androidx.recyclerview.widget.RecyclerView

import com.squareup.picasso.Picasso

import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.ImageView
import com.homework.moviescinema.R
import com.homework.moviescinema.detail_screen.data.model.Movie


class MovieAdapter : RecyclerView.Adapter<MovieViewHolder>() {
    private var movies: MutableList<Movie>
    private var onPosterClickListener: OnPosterClickListener? = null
    private var onReachEndListener: OnReachEndListener? = null

    interface OnPosterClickListener {
        fun onPosterClick(position: Int)
    }

    interface OnReachEndListener {
        fun onReachEnd()
    }

    fun setOnPosterClickListener(onPosterClickListener: OnPosterClickListener?) {
        this.onPosterClickListener = onPosterClickListener
    }

    fun setOnReachEndListener(onReachEndListener: OnReachEndListener?) {
        this.onReachEndListener = onReachEndListener
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MovieViewHolder {
        var view:View = LayoutInflater.from(viewGroup.context).inflate(R.layout.movie_item, viewGroup, false)
        return MovieViewHolder(view)
    }

    override fun onBindViewHolder(movieViewHolder: MovieViewHolder, i: Int) {
        if (i > movies.size - 4 && onReachEndListener != null) {
            onReachEndListener!!.onReachEnd()
        }
        val movie: Movie = movies[i]
        Picasso.get().load(movie.posterPath).into(movieViewHolder.imageViewSmallPoster)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    inner class MovieViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imageViewSmallPoster:ImageView = itemView.findViewById(R.id.imageViewSmallPoster)

        init {
            itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    if (onPosterClickListener != null) {
                        onPosterClickListener!!.onPosterClick(adapterPosition)
                    }
                }
            })
        }
    }

    fun setMovies(movies: MutableList<Movie>) {
        this.movies = movies
        notifyDataSetChanged()
    }

    fun addMovies(movies: List<Movie>?) {
        this.movies.addAll(movies!!)
        notifyDataSetChanged()
    }

    fun getMovies(): List<Movie> {
        return movies
    }

    fun clear() {
        movies.clear()
        notifyDataSetChanged()
    }

    init {
        movies = ArrayList()
    }
}