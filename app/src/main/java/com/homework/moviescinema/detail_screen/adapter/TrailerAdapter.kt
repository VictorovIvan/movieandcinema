package com.homework.moviescinema.detail_screen.adapter

import com.homework.moviescinema.detail_screen.data.model.Trailer

import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import androidx.annotation.NonNull
import com.homework.moviescinema.R


class TrailerAdapter : RecyclerView.Adapter<TrailerAdapter.TrailerViewHolder>() {
    private var trailers: ArrayList<Trailer>? = null
    private var onTrailerClickListener: OnTrailerClickListener? = null

    @NonNull
    override fun onCreateViewHolder(@NonNull viewGroup: ViewGroup, i: Int): TrailerViewHolder {
        val view: View =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.trailer_item, viewGroup, false)
        return TrailerViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull trailerViewHolder: TrailerViewHolder, i: Int) {
        val trailer = trailers!![i]
        trailerViewHolder.textViewNameOfVideo.text = trailer.name
    }

    override fun getItemCount(): Int {
        return trailers!!.size
    }

    interface OnTrailerClickListener {
        fun onTrailerClick(url: String?)
    }

    inner class TrailerViewHolder(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val textViewNameOfVideo: TextView

        init {
            textViewNameOfVideo = itemView.findViewById(R.id.textViewNameOfVideo)
            itemView.setOnClickListener(object : View.OnClickListener {
                override fun onClick(v: View?) {
                    if (onTrailerClickListener != null) {
                        onTrailerClickListener!!.onTrailerClick(trailers!![adapterPosition].key)
                    }
                }
            })
        }
    }

    fun setTrailers(trailers: ArrayList<Trailer>?) {
        this.trailers = trailers
        notifyDataSetChanged()
    }

    fun setOnTrailerClickListener(onTrailerClickListener: OnTrailerClickListener?) {
        this.onTrailerClickListener = onTrailerClickListener
    }
}