package com.homework.moviescinema.detail_screen.adapter

import android.widget.TextView

import androidx.recyclerview.widget.RecyclerView

import com.homework.moviescinema.detail_screen.data.model.Review

import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import androidx.annotation.NonNull
import com.homework.moviescinema.R


class ReviewAdapter : RecyclerView.Adapter<ReviewAdapter.ReviewViewHolder>() {
    private var reviews: ArrayList<Review>? = null

    @NonNull
    override fun onCreateViewHolder(@NonNull viewGroup: ViewGroup, i: Int): ReviewViewHolder {
        val view: View =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.review_item, viewGroup, false)
        return ReviewViewHolder(view)
    }

    override fun onBindViewHolder(@NonNull reviewViewHolder: ReviewViewHolder, i: Int) {
        val review = reviews!![i]
        reviewViewHolder.textViewContent.text = review.content
        reviewViewHolder.textViewAuthor.text = review.author
    }

    override fun getItemCount(): Int {
        return reviews!!.size
    }

    fun setReviews(reviews: ArrayList<Review>?) {
        this.reviews = reviews
        notifyDataSetChanged()
    }

    inner class ReviewViewHolder(@NonNull itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val textViewAuthor: TextView
        val textViewContent: TextView

        init {
            textViewAuthor = itemView.findViewById(R.id.textViewAuthor)
            textViewContent = itemView.findViewById(R.id.textViewContent)
        }
    }
}
