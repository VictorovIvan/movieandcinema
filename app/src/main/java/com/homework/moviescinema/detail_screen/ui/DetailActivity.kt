package com.homework.moviescinema.detail_screen.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import android.widget.ImageView
import android.widget.Toast

import com.squareup.picasso.Picasso

import com.homework.moviescinema.base_screen.ui.BaseViewModel

import androidx.lifecycle.ViewModelProviders
import android.widget.TextView
import com.homework.moviescinema.favourite_screen.data.model.FavouriteMovie
import com.homework.moviescinema.detail_screen.data.model.Movie
import com.homework.moviescinema.detail_screen.adapter.TrailerAdapter

import com.homework.moviescinema.detail_screen.adapter.ReviewAdapter
import androidx.recyclerview.widget.LinearLayoutManager

import android.view.View
import com.homework.moviescinema.detail_screen.adapter.TrailerAdapter.OnTrailerClickListener
import androidx.recyclerview.widget.RecyclerView
import com.homework.moviescinema.base_screen.ui.BaseActivity
import com.homework.moviescinema.R
import com.homework.moviescinema.favourite_screen.ui.FavouriteActivity
import com.homework.moviescinema.utils.JsonUtils
import com.homework.moviescinema.utils.NetworkUtils

class DetailActivity : AppCompatActivity() {
    lateinit var imageViewAddToFavourite:ImageView
    lateinit var imageViewBigPoster:ImageView
    lateinit var textViewTitle: TextView
    lateinit var textViewOriginalTitle: TextView
    lateinit var textViewRating: TextView
    lateinit var textViewReleaseDate: TextView
    lateinit var textViewOverview: TextView
    var id:Int = 0
    lateinit var movie: Movie
    private var favouriteMovie: FavouriteMovie? = null
    private var viewModel: BaseViewModel? = null

    lateinit var recyclerViewTrailers: RecyclerView
    lateinit var recyclerViewReviews: RecyclerView
    lateinit var reviewAdapter: ReviewAdapter
    lateinit var trailerAdapter: TrailerAdapter

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id: Int = item.getItemId()
        when (id) {
            R.id.itemMain -> {
                val intent = Intent(this, BaseActivity::class.java)
                startActivity(intent)
            }
            R.id.itemFavourite -> {
                val intentToFavourite = Intent(this, FavouriteActivity::class.java)
                startActivity(intentToFavourite)
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        val networkUtils = NetworkUtils
        val jsonUtils = JsonUtils()

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        imageViewBigPoster = findViewById(R.id.imageViewBigPoster)
        textViewTitle = findViewById(R.id.textViewTitle)
        textViewOriginalTitle = findViewById(R.id.textViewOriginalTitle)
        textViewRating = findViewById(R.id.textViewRating)
        textViewReleaseDate = findViewById(R.id.textViewReleaseDate)
        textViewOverview = findViewById(R.id.textViewOverview)
        imageViewAddToFavourite = findViewById(R.id.imageViewAddToFavourite)
       val intent = intent
       if (intent != null && intent.hasExtra("id")) {
            id = intent.getIntExtra("id", -1)
        } else {
            finish()
        }
        viewModel = ViewModelProviders.of(this).get(BaseViewModel::class.java)
        movie = viewModel!!.getMovieById(id)!!
        Picasso.get().load(movie?.bigPosterPath).into(imageViewBigPoster)
        textViewTitle.setText(movie?.title)
        textViewOriginalTitle.setText(movie.originalTitle)
        textViewOverview.setText(movie.overview)
        textViewReleaseDate.setText(movie.releaseDate)
        textViewRating.setText(java.lang.Double.toString(movie.voteAverage))
        setFavourite()
        recyclerViewTrailers = (findViewById<View>(R.id.recyclerViewTrailers) as RecyclerView?)!!
        recyclerViewReviews = (findViewById<View>(R.id.recyclerViewReviews) as RecyclerView?)!!
        reviewAdapter = ReviewAdapter()
        trailerAdapter = TrailerAdapter()
        trailerAdapter.setOnTrailerClickListener(object : OnTrailerClickListener {
            override fun onTrailerClick(url: String?) {
                val intentToTrailer = Intent(Intent.ACTION_VIEW, Uri.parse(url))
                startActivity(intentToTrailer)
            }
        })
        recyclerViewReviews.setLayoutManager(LinearLayoutManager(this))
        recyclerViewTrailers.setLayoutManager(LinearLayoutManager(this))
        recyclerViewReviews.setAdapter(reviewAdapter)
        recyclerViewTrailers.setAdapter(trailerAdapter)
        val jsonObjectTrailers = networkUtils.getJSONForVideos(movie.id)
        val jsonObjectReviews = networkUtils.getJSONForReviews(movie.id)
        val trailers = jsonUtils.getTrailersFromJSON(jsonObjectTrailers)
        val reviews = jsonUtils.getReviewsFromJSON(jsonObjectReviews)
        reviewAdapter.setReviews(reviews)
        trailerAdapter.setTrailers(trailers)
    }

    fun onClickChangeFavourite(view: android.view.View) {
        if (favouriteMovie == null) {
            viewModel?.insertFavouriteMovie(FavouriteMovie(movie))
            Toast.makeText(this, R.string.add_to_favourite, Toast.LENGTH_SHORT).show()
        } else {
            viewModel?.deleteFavouriteMovie(favouriteMovie)
            Toast.makeText(this, R.string.remove_from_favourite, Toast.LENGTH_SHORT).show()
        }
        setFavourite()
    }

    private fun setFavourite() {
        favouriteMovie = viewModel?.getFavouriteMovieById(id)
        if (favouriteMovie == null) {
            imageViewAddToFavourite.setImageResource(R.drawable.favourite_add_to)
        } else {
            imageViewAddToFavourite.setImageResource(R.drawable.favourite_remove)
        }
    }
}