package com.homework.moviescinema.detail_screen.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "movies")
open class Movie(
    @PrimaryKey(autoGenerate = true)
    var uniqueId:Int = 0,
    var id: Int,
    var voteCount: Int,
    var title: String,
    var originalTitle: String,
    var overview: String,
    var posterPath: String,
    var bigPosterPath: String,
    var backdropPath: String,
    var voteAverage: Double,
    var releaseDate: String
)