package com.homework.moviescinema.detail_screen.data.model

import androidx.room.Dao
import androidx.room.Delete

import androidx.lifecycle.LiveData
import androidx.room.Insert
import androidx.room.Query
import com.homework.moviescinema.detail_screen.data.model.Movie
import com.homework.moviescinema.favourite_screen.data.model.FavouriteMovie


@Dao
interface MovieDao {
    @get:Query("SELECT * FROM movies")
    val allMovies: LiveData<List<Movie?>?>?

    @get:Query("SELECT * FROM favourite_movies")
    val allFavouriteMovies: LiveData<List<FavouriteMovie?>?>?

    @Query("SELECT * FROM movies WHERE id == :movieId")
    fun getMovieById(movieId: Int): Movie?

    @Query("SELECT * FROM favourite_movies WHERE id == :movieId")
    fun getFavouriteMovieById(movieId: Int): FavouriteMovie?

    @Query("DELETE FROM movies")
    fun deleteAllMovies()

    @Insert
    fun insertMovie(movie: Movie?)

    @Delete
    fun deleteMovie(movie: Movie?)

    @Insert
    fun insertFavouriteMovie(movie: FavouriteMovie?)

    @Delete
    fun deleteFavouriteMovie(movie: FavouriteMovie?)
}
