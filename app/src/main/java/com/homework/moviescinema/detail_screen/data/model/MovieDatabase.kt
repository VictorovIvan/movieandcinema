package com.homework.moviescinema.detail_screen.data.model

import android.content.Context
import androidx.room.Room

import androidx.room.RoomDatabase

import androidx.room.Database
import com.homework.moviescinema.favourite_screen.data.model.FavouriteMovie


@Database(entities = [Movie::class, FavouriteMovie::class], version = 5, exportSchema = false)
abstract class MovieDatabase : RoomDatabase() {
    abstract fun movieDao(): MovieDao?

    companion object {
        private const val DB_NAME = "movies.db"
        private var database: MovieDatabase? = null
        private val LOCK = Any()
        fun getInstance(context: Context?): MovieDatabase? {
            synchronized(LOCK) {
                if (database == null) {
                    database = Room.databaseBuilder(
                        context!!,
                        MovieDatabase::class.java, DB_NAME
                    ).fallbackToDestructiveMigration().build()
                }
            }
            return database
        }
    }
}
