package com.homework.moviescinema.detail_screen.domain.model

import androidx.room.PrimaryKey

class MoviesDomainModel (
    var uniqueId:Int = 0,
    var id: Int,
    var voteCount: Int,
    var title: String,
    var originalTitle: String,
    var overview: String,
    var posterPath: String,
    var bigPosterPath: String,
    var backdropPath: String,
    var voteAverage: Double,
    var releaseDate: String
)