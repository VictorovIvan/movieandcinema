package com.homework.moviescinema.detail_screen.data.model

class Review(var author: String, var content: String)